import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  
  static targets = ['image', 'container']
  static values = {
    hmax: {type: Number, default: 320}
  }
  
  connect() {
    if (this.isLoaded) {
      this.resize()
    }
  }

  get isLoaded() {
    return this.imageTarget.complete
  }
  
  resize() {
    //console.log(this.hmaxValue)
    var imageWidth = this.imageTarget.offsetWidth
    var imageHeight = this.imageTarget.offsetHeight
    this.containerTarget.style.width = imageWidth*this.hmaxValue/imageHeight + 'px'
    this.containerTarget.style.flexGrow = imageWidth*this.hmaxValue/imageHeight
  }

  disconnect () {
    this.containerTarget.style.removeProperty("width")
    this.containerTarget.style.removeProperty("flexGrow")
  }

}