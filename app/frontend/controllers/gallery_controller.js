import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static targets = ['image', 'container']
  
  connect() {
    //console.log(document.querySelectorAll('.thumbnail'))
    //var thumbnails = document.querySelectorAll('.thumbnail')
    //thumbnails.forEach(function(element) {
    //  console.log(element.thumbnailController)
    //})
    //this.element.thumbnail.resize()
    var imageWidths = []
    var imageHeights = []
    //console.log(this.imageTargets.length)
    this.imageTargets.forEach(element => {
      //console.log(element.offsetWidth)
      imageWidths.push(element.offsetWidth)
      imageHeights.push(element.offsetHeight)
      //console.log(imageWidths)
    })
    for (let i = 0 ; i < this.imageTargets.length; i++) {
      this.containerTargets[i].style.width = imageWidths[i]*256/imageHeights[i] + 'px'
      this.containerTargets[i].style.flexGrow = imageWidths[i]*256/imageHeights[i]
    }
  }
  
  resize(imageWidth, imageHeight) {
    this.containerTarget.style.width = imageWidth*256/imageHeight + 'px'
    this.containerTarget.style.flexGrow = imageWidth*256/imageHeight
  }
  
  containerTargetConnected(element) {
    console.log(this.containerTarget)
  }
}