import { Controller } from "@hotwired/stimulus"
import { useHotkeys } from 'stimulus-use'

export default class extends Controller {
  static targets = ['image']
  static values = {
    previousPhoto: String,
    nextPhoto: String,
    gallery: String
  }
  
  connect() {
    //console.log(this.imageTarget.src)
    //console.log(this.imageTarget.dataset.src)
    
    useHotkeys(this, {
      left: [this.previousPhoto],
      right: [this.nextPhoto],
      esc: [this.backToGallery]
    })

  }
  
  imageTargetConnected() {
    this.preventDownload()
  }
  
  get isLoaded() {
    return this.imageTarget.complete
  }
  
  switchSrc() {
    this.imageTarget.src = this.imageTarget.dataset.src
  }
  
  preventDownload() {
    this.imageTarget.oncontextmenu = (e) => {
      e.preventDefault();
    }
  }
  
  previousPhoto(event) {
    console.log(this.previousPhotoValue)
    Turbo.visit(this.previousPhotoValue)
  }
  
  nextPhoto(event) {
    console.log(this.nextPhotoValue)
    Turbo.visit(this.nextPhotoValue)
  }
  
  backToGallery(event) {
    Turbo.visit(this.galleryValue)
  }
}