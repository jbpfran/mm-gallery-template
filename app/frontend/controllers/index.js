// stimulus 3.0
import { Application } from "@hotwired/stimulus"
import { registerControllers } from 'stimulus-vite-helpers'

window.Stimulus = Application.start()
//const context = require.context("./controllers", true, /\.js$/)
//Stimulus.load(definitionsFromContext(context))
const controllers = import.meta.globEager('./**/*_controller.js')
registerControllers(Stimulus, controllers)

// enable Stimulus debug mode in development
Stimulus.debug = process.env.NODE_ENV === 'development'