class Photo
  def self.list(album)
    Dir.glob(album + "/**/*.jpg")
  end
  
  def self.all
    Dir.glob("source/images/galleries/**/*.jpg")
  end
end