class Album
  def self.list
    Dir.glob("source/images/galleries/*")
  end
  
  def self.cover_page(folder)
    folder.split('/').last + '-cover'
  end
  
  def self.photos_page(folder)
    folder.split('/').last + '-photos'
  end
  
  def self.photos_frame(folder)
    folder.split('/').last + '-frame'
  end
end