# Gallery Template for Middleman
Build a gallery / portfolio website with Middleman.

## How to setup

Create your project using this git

    middleman init project_name -T jbpfran/mm-gallery-template

    cd project_name

    bundle install
    npm install

Launch the development server with `./bin/dev`

Launch the build process with `bundle exec middleman build`

## Configuration

You need to organize your photos into albums (directories) in the `source/images/galleries` directory. Each sub directory will create a new album.

You'll also have to include a `gallery-cover.jpg` in the `images` directory for your index page.

Most of the configuration is then in the frontmatter of the template pages.

```
title: My Fantastic Gallery
description: My Fantastic Gallery
portfolio_title: My Fantastic Gallery
portfolio_subtitle: My Fantastic Gallery
portfolio_location: My Fantastic Gallery
layout: cover
```

The config.rb can also be (heavily) reviewed to fit your needs.

If you want to deploy on Gitlab Pages don't forget to change `.gitlab-ci.yml` with your username and project name and `config.rb` with your project name.

The middleman-images extension needs extra installation if you want to fully use it
https://github.com/toy/image_optim_pack


## Prerequisites :  

- Ruby 2.7.2  
- Middleman 4.4.2  
- NPM

It's a skeleton template for Middleman including :

- external asset pipeline managed by Vite
- [Turbo](https://turbo.hotwired.dev)
- [Stimulus 3](https://stimulus.hotwired.dev)
- [Tailwind CSS](https://tailwindcss.com)
- SASS
- HAML as default templating
- Gitlab CI to publish to Gitlab Pages

I tried to organize it kind of like a rails app.  
Feel free to comment and improve.

Thanks to Gitlab template for Middleman regarding the CI configuration.

## Contributions
Feel free to take it, use it, modify it.

1. Fork it
2. yarn install && bundle install
3. Create your feature branch (git checkout -b feature/my-new-feature)
4. Commit your changes (git commit -am 'Added some feature')
5. Push to the branch (git push origin my-new-feature)
6. Create new Pull Request