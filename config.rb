# Vite Configuration
require "vite_ruby"
require "vite_padrino/tag_helpers"
helpers VitePadrino::TagHelpers

# Configuring PORO
$LOAD_PATH.unshift("#{File.dirname(__FILE__)}/lib")

# Helpers
# helpers are in the helpers directory at the project root
$LOAD_PATH.unshift("#{File.dirname(__FILE__)}/app/helpers")
Dir['./app/helpers/*.rb'].each { |file| load file }
helpers MarkdownHelper
helpers LocaleHelper
helpers ImagesHelper
helpers FaviconsHelper
helpers PhotosHelper

# Set favicons
set :favicons, [
  {
    rel: 'apple-touch-icon',
    size: '180x180',
    icon: 'apple-touch-icon.png'
  },
  {
    rel: 'icon',
    type: 'image/png',
    size: '32x32',
    icon: 'favicon32x32.png'
  },
  {
    rel: 'icon',
    type: 'image/png',
    size: '16x16',
    icon: 'favicon16x16.png'
  },
  {
    rel: 'shortcut icon',
    size: '64x64,32x32,24x24,16x16',
    icon: 'favicon.ico'
  }
]

# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

#activate :autoprefixer do |prefix|
#  prefix.browsers = "last 2 versions"
#end

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# HAML as default template
set :haml, { :format => :html5 }
Haml::TempleEngine.disable_option_validator!

# accept front matter from html and haml files
set :frontmatter_extensions, %w(.html .haml)

# changing markdown engine to redcarpet
activate :syntax
set :markdown_engine, :redcarpet
set :markdown, :tables => true, :footnotes => true, :smartypants => true, :autolink => true, :superscript => true, :fenced_code_blocks => true

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

require "album"
require "photo"

# Albums Cover
Album.list.each do |album|
  proxy Album.cover_page(album).concat('.html'), "/templates/album-cover.html", locals: {album: album}
end

# Albums Photos
Album.list.each do |album|
  proxy Album.photos_page(album).concat('.html'), "/templates/album-photos.html", locals: {album: album}
end

# Albums photos - turbo frame
Album.list.each do |album|
  proxy '/albums/' + Album.photos_frame(album).concat('.html'), "/templates/album-photos-frame.html", locals: {album: album}
end

# Photo view
Album.list.each do |album|
  Photo.list(album).each do |photo|
    #index = Photo.list(album).index(photo)
    proxy '/album/' + photo.delete_prefix("source/images/galleries/").gsub(/(jpg|jpeg|JPG)/, 'html'), "/templates/photo-show.html", locals: {album: album, photo: photo}
  end
end

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# helpers do
#   def some_helper
#     'Helping'
#   end
# end

helpers do
  def asset_path(*args)
    if args.size == 1
      super(File.extname(args[0]).delete(".").to_sym, args[0])
    else
      super(*args)
    end
  end
end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

# configure :build do
#   activate :minify_css
#   activate :minify_javascript
# end

configure :development do
  use ViteRuby::DevServerProxy, ssl_verify_none: true
  activate :livereload
  activate :images
end

configure :build do
  ignore 'templates/*'
  
  if ENV['DEPLOY'] == 'Gitlab'
    # Dedicated config to build with Gitlab CI
    set :build_dir, 'public'
    set :base_url, "project_name"
    set :http_prefix, "/project_name"
    #activate :relative_assets
  else
    # standard deployment
    activate :images do |images|
      # Do not include original images in the build (default: false)
      images.ignore_original = true

      # Specify another cache directory depending on your root directory (default: 'cache')
      images.cache_dir = 'tmp'

      # Optimize images (default: false)
      images.optimize = true

      # Provide additional options for image_optim
      # See https://github.com/toy/image_optim for all available options
      images.image_optim = {
        nice: 20,
        optipng: {
          level: 5,
        },
      }
    end
    #activate :gzip
  end
  
end