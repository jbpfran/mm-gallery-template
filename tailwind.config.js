module.exports = {
  content: [
    "./source/**/*.{html,haml}",
    "./app/frontend/**/*.{js,ts,css,sass,scss}"
  ],
  theme: {
    extend: {
      height: {
        sm: '320px',
        md: '576px',
        lg: '720px',
        xl: '1024px',
      },
      maxHeight: {
        '0': '0',
        '1/4': '25vh',
        '1/2': '50vh',
        '3/4': '75vh',
        '4/5': '80vh',
        'full': '100vh',
      }
    },
  },
  variants: {
    borderWidth: ['hover', 'focus'],
  },
  plugins: [
    require('@tailwindcss/aspect-ratio'),
  ],
}